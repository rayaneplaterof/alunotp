const express = require('express');
const app = express();
const PORT = 8000;

app.use(express.urlencoded({ extended: true }));

app.post('/pets/inserirAnimal', (req, res) => {
    console.log(req.body);
    let id = req.body.id;
    let nome = req.body.nome;
    let raca = req.body.raca;
    let idade = req.body.idade;

    if (!id && nome != "" && raca != "" && idade > 0.0)
        res.status(201).send("Animal criado com sucesso");

    res.status(401).send("Erro ao criar animal");
});

app.get('/pets/pesquisarAnimal', (req, res) => {

    let nome = req.query.nome;

    if (!nome)
        res.status(400).send("Nome não encontrado!");

    let retBanco = JSON.stringify(req.query);
    res.status(200).send(retBanco);

});

app.put('/pets/inativarAnimal', (req, res) => {
    console.log(req.body);
    let id = req.body.id;

    if (!id)
        res.status(201).send("Animal inativado com sucesso");

    res.status(401).send("Erro em inativar animal");
});

app.put('/pets/atualizarAnimalCompleto', (req, res) => {
    console.log(req.body);
    let id = req.body.id;
    let nome = req.body.nome;
    let raca = req.body.raca;
    let idade = req.body.idade;

    if (!id && nome != "" || raca != "" || idade > 0.0)
        res.status(201).send("Animal atualizado com sucesso");

    res.status(401).send("Erro em atualizar animal");
});

app.listen(PORT, () => {
    console.log(`PROJETO INICIADO NA PORTA ${PORT}`);
});